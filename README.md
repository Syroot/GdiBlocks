# GdiBlocks

A very silly Tetris clone for Windows written with GDI+ in a few hours with the following features:

- Configurable board width (6-250) and height (7-125)
- Configurable startup and maximum speed (increased for each full line)
- Extended tetromino set (U, +, long S, long Z and corner)
- Drop ghost display.

![Sample Game](https://gitlab.com/Syroot/GdiBlocks/raw/master/doc/readme/sample.png)
