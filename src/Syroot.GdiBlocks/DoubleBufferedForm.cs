﻿using System.Windows.Forms;

namespace Syroot.GdiBlocks
{
    /// <summary>
    /// A window slightly optimized for GDI graphics output.
    /// </summary>
    internal class DoubleBufferedForm : Form
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="DoubleBufferedForm"/> class.
        /// </summary>
        internal DoubleBufferedForm()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
        }
    }
}
