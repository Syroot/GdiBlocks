﻿namespace Syroot.GdiBlocks
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this._btnStart = new System.Windows.Forms.Button();
            this._lblBoardWidth = new System.Windows.Forms.Label();
            this._lblBoardHeight = new System.Windows.Forms.Label();
            this._nudBoardWidth = new System.Windows.Forms.NumericUpDown();
            this._nudBoardHeight = new System.Windows.Forms.NumericUpDown();
            this._lblStartupSpeed = new System.Windows.Forms.Label();
            this._nudStartupSpeed = new System.Windows.Forms.NumericUpDown();
            this._chkExtendedTetrominoes = new System.Windows.Forms.CheckBox();
            this._lblMaximumSpeed = new System.Windows.Forms.Label();
            this._nudMaximumSpeed = new System.Windows.Forms.NumericUpDown();
            this._chkGhostTile = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this._nudBoardWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nudBoardHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nudStartupSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nudMaximumSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // _btnStart
            // 
            this._btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnStart.Location = new System.Drawing.Point(293, 117);
            this._btnStart.Name = "_btnStart";
            this._btnStart.Size = new System.Drawing.Size(75, 23);
            this._btnStart.TabIndex = 10;
            this._btnStart.Text = "Start";
            this._btnStart.UseVisualStyleBackColor = true;
            this._btnStart.Click += new System.EventHandler(this._btnStart_Click);
            // 
            // _lblBoardWidth
            // 
            this._lblBoardWidth.AutoSize = true;
            this._lblBoardWidth.Location = new System.Drawing.Point(12, 14);
            this._lblBoardWidth.Name = "_lblBoardWidth";
            this._lblBoardWidth.Size = new System.Drawing.Size(73, 15);
            this._lblBoardWidth.TabIndex = 0;
            this._lblBoardWidth.Text = "Board Width";
            // 
            // _lblBoardHeight
            // 
            this._lblBoardHeight.AutoSize = true;
            this._lblBoardHeight.Location = new System.Drawing.Point(193, 14);
            this._lblBoardHeight.Name = "_lblBoardHeight";
            this._lblBoardHeight.Size = new System.Drawing.Size(77, 15);
            this._lblBoardHeight.TabIndex = 2;
            this._lblBoardHeight.Text = "Board Height";
            // 
            // _nudBoardWidth
            // 
            this._nudBoardWidth.Location = new System.Drawing.Point(112, 12);
            this._nudBoardWidth.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this._nudBoardWidth.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this._nudBoardWidth.Name = "_nudBoardWidth";
            this._nudBoardWidth.Size = new System.Drawing.Size(75, 23);
            this._nudBoardWidth.TabIndex = 1;
            this._nudBoardWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._nudBoardWidth.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // _nudBoardHeight
            // 
            this._nudBoardHeight.Location = new System.Drawing.Point(293, 12);
            this._nudBoardHeight.Maximum = new decimal(new int[] {
            125,
            0,
            0,
            0});
            this._nudBoardHeight.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this._nudBoardHeight.Name = "_nudBoardHeight";
            this._nudBoardHeight.Size = new System.Drawing.Size(75, 23);
            this._nudBoardHeight.TabIndex = 3;
            this._nudBoardHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._nudBoardHeight.Value = new decimal(new int[] {
            24,
            0,
            0,
            0});
            // 
            // _lblStartupSpeed
            // 
            this._lblStartupSpeed.AutoSize = true;
            this._lblStartupSpeed.Location = new System.Drawing.Point(12, 43);
            this._lblStartupSpeed.Name = "_lblStartupSpeed";
            this._lblStartupSpeed.Size = new System.Drawing.Size(80, 15);
            this._lblStartupSpeed.TabIndex = 4;
            this._lblStartupSpeed.Text = "Startup Speed";
            // 
            // _nudStartupSpeed
            // 
            this._nudStartupSpeed.Location = new System.Drawing.Point(112, 41);
            this._nudStartupSpeed.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this._nudStartupSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._nudStartupSpeed.Name = "_nudStartupSpeed";
            this._nudStartupSpeed.Size = new System.Drawing.Size(75, 23);
            this._nudStartupSpeed.TabIndex = 5;
            this._nudStartupSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._nudStartupSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // _chkExtendedTetrominoes
            // 
            this._chkExtendedTetrominoes.AutoSize = true;
            this._chkExtendedTetrominoes.Location = new System.Drawing.Point(15, 70);
            this._chkExtendedTetrominoes.Name = "_chkExtendedTetrominoes";
            this._chkExtendedTetrominoes.Size = new System.Drawing.Size(170, 19);
            this._chkExtendedTetrominoes.TabIndex = 8;
            this._chkExtendedTetrominoes.Text = "Use extended tetromino set";
            this._chkExtendedTetrominoes.UseVisualStyleBackColor = true;
            // 
            // _lblMaximumSpeed
            // 
            this._lblMaximumSpeed.AutoSize = true;
            this._lblMaximumSpeed.Location = new System.Drawing.Point(193, 43);
            this._lblMaximumSpeed.Name = "_lblMaximumSpeed";
            this._lblMaximumSpeed.Size = new System.Drawing.Size(96, 15);
            this._lblMaximumSpeed.TabIndex = 6;
            this._lblMaximumSpeed.Text = "Maximum Speed";
            // 
            // _nudMaximumSpeed
            // 
            this._nudMaximumSpeed.Location = new System.Drawing.Point(293, 41);
            this._nudMaximumSpeed.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this._nudMaximumSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._nudMaximumSpeed.Name = "_nudMaximumSpeed";
            this._nudMaximumSpeed.Size = new System.Drawing.Size(75, 23);
            this._nudMaximumSpeed.TabIndex = 7;
            this._nudMaximumSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._nudMaximumSpeed.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this._nudMaximumSpeed.ValueChanged += new System.EventHandler(this._nudMaximumSpeed_ValueChanged);
            // 
            // _chkGhostTile
            // 
            this._chkGhostTile.AutoSize = true;
            this._chkGhostTile.Checked = true;
            this._chkGhostTile.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkGhostTile.Location = new System.Drawing.Point(15, 95);
            this._chkGhostTile.Name = "_chkGhostTile";
            this._chkGhostTile.Size = new System.Drawing.Size(107, 19);
            this._chkGhostTile.TabIndex = 9;
            this._chkGhostTile.Text = "Show ghost tile";
            this._chkGhostTile.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AcceptButton = this._btnStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 152);
            this.Controls.Add(this._chkGhostTile);
            this.Controls.Add(this._chkExtendedTetrominoes);
            this.Controls.Add(this._nudMaximumSpeed);
            this.Controls.Add(this._nudStartupSpeed);
            this.Controls.Add(this._lblMaximumSpeed);
            this.Controls.Add(this._lblStartupSpeed);
            this.Controls.Add(this._nudBoardHeight);
            this.Controls.Add(this._nudBoardWidth);
            this.Controls.Add(this._lblBoardHeight);
            this.Controls.Add(this._lblBoardWidth);
            this.Controls.Add(this._btnStart);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GdiBlocks Settings";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.FormMain_HelpButtonClicked);
            ((System.ComponentModel.ISupportInitialize)(this._nudBoardWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nudBoardHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nudStartupSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nudMaximumSpeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _btnStart;
        private System.Windows.Forms.Label _lblBoardWidth;
        private System.Windows.Forms.Label _lblBoardHeight;
        private System.Windows.Forms.NumericUpDown _nudBoardWidth;
        private System.Windows.Forms.NumericUpDown _nudBoardHeight;
        private System.Windows.Forms.Label _lblStartupSpeed;
        private System.Windows.Forms.NumericUpDown _nudStartupSpeed;
        private System.Windows.Forms.CheckBox _chkExtendedTetrominoes;
        private System.Windows.Forms.Label _lblMaximumSpeed;
        private System.Windows.Forms.NumericUpDown _nudMaximumSpeed;
        private System.Windows.Forms.CheckBox _chkGhostTile;
    }
}