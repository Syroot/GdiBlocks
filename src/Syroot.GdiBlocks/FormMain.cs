﻿using System;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

namespace Syroot.GdiBlocks
{
    /// <summary>
    /// Start-up form which provides an options dialog with which the game will be started.
    /// </summary>
    public partial class FormMain : Form
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FormMain"/> class.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void FormMain_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"{Application.ProductName} {Application.ProductVersion}");
            builder.AppendLine("Copyright © 2010-2018 Syroot");
            builder.Append("My thumb's on a tetris keyring");
            MessageBox.Show(builder.ToString(), "About GdiBlocks", MessageBoxButtons.OK, MessageBoxIcon.Information);
            e.Cancel = true;
        }

        private void _nudMaximumSpeed_ValueChanged(object sender, EventArgs e)
        {
            _nudStartupSpeed.Maximum = _nudMaximumSpeed.Value;
        }

        private void _btnStart_Click(object sender, EventArgs e)
        {
            GameSettings settings = new GameSettings()
            {
                BoardWidth = (int)_nudBoardWidth.Value,
                BoardHeight = (int)_nudBoardHeight.Value,
                ExtendedTetrominoes = _chkExtendedTetrominoes.Checked,
                GhostTile = _chkGhostTile.Checked,
                MaximumSpeed = (int)_nudMaximumSpeed.Value,
                StartupSpeed = (int)_nudStartupSpeed.Value
            };
            Game game = new Game(settings);
            game.FormClosed += game_FormClosed;
            game.Run();
        }

        private void game_FormClosed(object sender, FormClosedEventArgs e)
        {
            BringToFront();
        }
    }
}
