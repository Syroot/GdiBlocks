﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Syroot.GdiBlocks.Properties;

namespace Syroot.GdiBlocks
{
    /// <summary>
    /// A Tetris attempt with an one-dimensional array. Does not support SRS.
    /// </summary>
    internal class Game
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _vanishRows = 3;
        private const int _maxDelay = 1000;
        private const int _delayStep = 8;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private DoubleBufferedForm _form;

        private GameSettings _settings;
        private readonly int _minDelay;

        // Tetrominos and game board
        private List<int[][]> _tetrominoes; // [Rotation][Offsets]
        private int[][][] _tetrominoJLSTZKickOffsets; // [Rotation][Direction][Tests]
        private int[][][] _tetrominoIKickOffsets; // [Rotation][Direction][Tests]
        private BlockState[] _board;

        private int _tetrominoIndex;
        private int _tetrominoRotation;
        private int _tetrominoOffset;
        private int _dropOffset;

        // Game control
        private Random _random;
        private Timer _timer;
        private bool _paused;
        private bool _pausedThroughDeactivation;

        // Graphics
        private int _blockSize;
        private Bitmap _bitmap;
        private Graphics _graphics;

        // Score keeping
        private int _score;
        private int _blocks;
        private int _lines;
        private int _delay;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new game round with the specified settings.
        /// </summary>
        /// <param name="settings">The settings of the game.</param>
        internal Game(GameSettings settings)
        {
            // Create objects dependent on settings.
            _settings = settings;
            _minDelay = _maxDelay - settings.MaximumSpeed * 10;

            // Initialize the game.
            _random = new Random();
            CreateForm();
            CreateBoard();
            CreateTetrominoes();
            CreateTetrominoKickOffsets();

            // Initialize the timers.
            _timer = new Timer();
            SetDelay(_maxDelay - settings.StartupSpeed * 10);
            _timer.Tick += _timer_Tick;
        }

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Raised when the game form closes (e.g. a GameOver happened).
        /// </summary>
        internal event FormClosedEventHandler FormClosed
        {
            add { _form.FormClosed += value; }
            remove { _form.FormClosed -= value; }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Runs the game which shows the window and starts the timer.
        /// </summary>
        internal void Run()
        {
            NewTetromino();

            _form.Show();
            _timer.Enabled = true;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        // ---- Initialization ----

        private void CreateForm()
        {
            _form = new DoubleBufferedForm
            {
                BackColor = Color.Black,
                ClientSize = new Size(_settings.BoardWidth * 24, (_settings.BoardHeight - _vanishRows) * 24),
                Icon = Resources.Icon,
                StartPosition = FormStartPosition.CenterScreen,
                WindowState = FormWindowState.Maximized
            };
            _form.KeyDown += _form_KeyDown;
            _form.Paint += _form_Paint;
            _form.SizeChanged += _form_SizeChanged;
            _form.GotFocus += _form_GotFocus;
            _form.LostFocus += _form_LostFocus;
        }

        private void CreateBoard()
        {
            _board = new BlockState[_settings.BoardWidth * _settings.BoardHeight];
            // Left and right borders.
            for (int i = 0; i < _board.Length; i += _settings.BoardWidth)
            {
                _board[i] = BlockState.Border;
                _board[i + _settings.BoardWidth - 1] = BlockState.Border;
            }
            // Bottom border.
            for (int i = _board.Length - _settings.BoardWidth + 1; i < _board.Length - 1; i++)
            {
                _board[i] = BlockState.Border;
            }
            ResizeBoard();
        }

        private void CreateTetrominoes()
        {
            int r0 = 0;
            int r1 = _settings.BoardWidth;
            int r2 = _settings.BoardWidth * 2;
            int r3 = _settings.BoardWidth * 3;

            _tetrominoes = new List<int[][]>
            {
                // I
                new int[][]
                {
                    new int[] { 0 + r1, 1 + r1, 2 + r1, 3 + r1 },
                    new int[] { 2 + r0, 2 + r1, 2 + r2, 2 + r3 },
                    new int[] { 0 + r2, 1 + r2, 2 + r2, 3 + r2 },
                    new int[] { 1 + r0, 1 + r1, 1 + r2, 1 + r3 }
                },
                // J
                new int[][]
                {
                    new int[] { 0 + r0, 0 + r1, 1 + r1, 2 + r1 },
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 1 + r2 },
                    new int[] { 0 + r1, 1 + r1, 2 + r1, 2 + r2 },
                    new int[] { 1 + r0, 1 + r1, 0 + r2, 1 + r2 }
                },
                // L
                new int[][]
                {
                    new int[] { 2 + r0, 0 + r1, 1 + r1, 2 + r1 },
                    new int[] { 1 + r0, 1 + r1, 1 + r2, 2 + r2 },
                    new int[] { 0 + r1, 1 + r1, 2 + r1, 0 + r2 },
                    new int[] { 0 + r0, 1 + r0, 1 + r1, 1 + r2 }
                },
                // O
                new int[][]
                {
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 2 + r1 },
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 2 + r1 },
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 2 + r1 },
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 2 + r1 }
                },
                // S
                new int[][]
                {
                    new int[] { 1 + r0, 2 + r0, 0 + r1, 1 + r1 },
                    new int[] { 1 + r0, 1 + r1, 2 + r1, 2 + r2 },
                    new int[] { 1 + r1, 2 + r1, 0 + r2, 1 + r2 },
                    new int[] { 0 + r0, 0 + r1, 1 + r1, 1 + r2 }
                },
                // T
                new int[][]
                {
                    new int[] { 1 + r0, 0 + r1, 1 + r1, 2 + r1 },
                    new int[] { 1 + r0, 1 + r1, 2 + r1, 1 + r2 },
                    new int[] { 0 + r1, 1 + r1, 2 + r1, 1 + r2 },
                    new int[] { 1 + r0, 0 + r1, 1 + r1, 1 + r2 }
                },
                // Z
                new int[][]
                {
                    new int[] { 0 + r0, 1 + r0, 1 + r1, 2 + r1 },
                    new int[] { 2 + r0, 1 + r1, 2 + r1, 1 + r2 },
                    new int[] { 0 + r1, 1 + r1, 1 + r2, 2 + r2 },
                    new int[] { 1 + r0, 0 + r1, 1 + r1, 0 + r2 }
                },
                // Extended blocks
                // U
                new int[][]
                {
                    new int[] { 0 + r0, 2 + r0, 0 + r1, 1 + r1, 2 + r1 },
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 1 + r2, 2 + r2 },
                    new int[] { 0 + r1, 1 + r1, 2 + r1, 0 + r2, 2 + r2 },
                    new int[] { 0 + r0, 1 + r0, 1 + r1, 0 + r2, 1 + r2 }
                },
                // +
                new int[][]
                {
                    new int[] { 1 + r0, 0 + r1, 1 + r1, 2 + r1, 1 + r2 },
                    new int[] { 1 + r0, 0 + r1, 1 + r1, 2 + r1, 1 + r2 },
                    new int[] { 1 + r0, 0 + r1, 1 + r1, 2 + r1, 1 + r2 },
                    new int[] { 1 + r0, 0 + r1, 1 + r1, 2 + r1, 1 + r2 }
                },
                // ZicZacJ
                new int[][]
                {
                    new int[] { 0 + r0, 0 + r1, 1 + r1, 2 + r1, 2 + r2 },
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 0 + r2, 1 + r2 },
                    new int[] { 0 + r0, 0 + r1, 1 + r1, 2 + r1, 2 + r2 },
                    new int[] { 1 + r0, 2 + r0, 1 + r1, 0 + r2, 1 + r2 }
                },
                // ZicZacL
                new int[][]
                {
                    new int[] { 2 + r0, 0 + r1, 1 + r1, 2 + r1, 0 + r2 },
                    new int[] { 0 + r0, 1 + r0, 1 + r1, 1 + r2, 2 + r2 },
                    new int[] { 2 + r0, 0 + r1, 1 + r1, 2 + r1, 0 + r2 },
                    new int[] { 0 + r0, 1 + r0, 1 + r1, 1 + r2, 2 + r2 }
                },
                // Corner
                new int[][]
                {
                    new int[] { 1 + r0, 0 + r1, 1 + r1 },
                    new int[] { 1 + r0, 1 + r1, 2 + r1 },
                    new int[] { 1 + r1, 2 + r1, 1 + r2 },
                    new int[] { 0 + r1, 1 + r1, 1 + r2 }
                }
            };
        }

        private void CreateTetrominoKickOffsets()
        {
            int r1 = _settings.BoardWidth;
            int r2 = _settings.BoardWidth * 2;

            _tetrominoJLSTZKickOffsets = new int[][][]
            {
                // 0° rotation
                new int[][]
                {
                    new int[] { -1, -1 + r1, 0 - r2, -1 - r2 }, // Rotate to 90° cw / right
                    new int[] {  1,  1 + r1, 0 - r2,  1 - r2 }  // Rotate to 270° ccw / left
                },
                // 90° rotation
                new int[][]
                {
                    new int[] {  1,  1 - r1, 0 + r2,  1 + r2 }, // Rotate to 180° cw / right
                    new int[] {  1,  1 - r1, 0 + r2,  1 + r2 }  // Rotate to 0° ccw / left
                },
                // 180° rotation
                new int[][]
                {
                    new int[] {  1,  1 + r1, 0 - r2,  1 - r2 }, // Rotate to 270° cw / right
                    new int[] { -1, -1 + r1, 0 - r2, -1 - r2 }  // Rotate to 90° ccw / left
                },
                // 270° rotation
                new int[][]
                {
                    new int[] { -1, -1 - r1, 0 + r2, -1 + r2 }, // Rotate to 0° cw / right
                    new int[] { -1, -1 - r1, 0 + r2, -1 + r2 }  // Rotate to 180° ccw / left
                }
            };

            _tetrominoIKickOffsets = new int[][][]
            {
                // 0° rotation
                new int[][]
                {
                    new int[] { -2,  1, -2 - r1,  1 + r2 }, // Rotate to 90° cw / right
                    new int[] { -1,  2, -1 + r2,  2 - r1 }  // Rotate to 270° ccw / left
                },
                // 90° rotation
                new int[][]
                {
                    new int[] {  1,  2, -1 + r2,  2 - r1 }, // Rotate to 180° cw / right
                    new int[] {  2, -1, +2 + r1, -1 - r2 }  // Rotate to 0° ccw / left
                },
                // 180° rotation
                new int[][]
                {
                    new int[] {  2, -1,  2 + r1, -1 - r2 }, // Rotate to 270° cw / right
                    new int[] {  1, -2,  1 - r2, -2 + r1 }  // Rotate to 90° ccw / left
                },
                // 270° rotation
                new int[][]
                {
                    new int[] {  1, -2,  1 - r2, -2 + r1 }, // Rotate to 0° cw / right
                    new int[] { -2,  1, -2 - r1,  1 + r2 }  // Rotate to 180° ccw / left
                }
            };
        }

        // ---- Updating ----

        private void NewTetromino()
        {
            _blocks++;

            // A tetromino starts after the vanish zone.
            int maxIndex = _settings.ExtendedTetrominoes ? 12 : 7;
            _tetrominoIndex = _random.Next(0, maxIndex);
            _tetrominoOffset = _settings.BoardWidth / 2 - 2 + _vanishRows * _settings.BoardWidth;
            _tetrominoRotation = 0;

            // If tetromino can't be placed, kill the game.
            if (CheckTetrominoCollision(_tetrominoIndex, _tetrominoRotation, _tetrominoOffset))
                GameOver();
            else
                UpdateDropTetromino();
        }

        private void MoveTetromino(int direction)
        {
            // Check if it can go into the direction.
            if (!CheckTetrominoCollision(_tetrominoIndex, _tetrominoRotation, _tetrominoOffset + direction))
            {
                _tetrominoOffset += direction;
                UpdateDropTetromino();
                DrawBoard();
            }
        }

        private void UpdateDropTetromino()
        {
            // Get the lowest position the current tetromino can get (drop position).
            for (int i = _tetrominoOffset; i < _board.Length; i += _settings.BoardWidth)
            {
                if (CheckTetrominoCollision(_tetrominoIndex, _tetrominoRotation, i))
                    break;
                else
                    _dropOffset = i;
            }
        }

        private void RotateTetromino(int direction)
        {
            // Get the next rotation index.
            int nextRotation = _tetrominoRotation + direction;
            if (nextRotation == -1)
                nextRotation = _tetrominoes[_tetrominoIndex].Length - 1;
            else if (nextRotation > 3)
                nextRotation = 0;

            // Check which rotation will be possible, try one without kicks first.
            int nextOffset = 0;
            bool canRotate = !CheckTetrominoCollision(_tetrominoIndex, nextRotation,
                _tetrominoOffset);

            // If the block can't be rotated, try it with the SRS kick offsets if it has some.
            if (!canRotate)
            {
                // Get the SRS rotation offsets for the current block.
                int[][][] kickOffsets = null;
                switch ((BlockState)(_tetrominoIndex + 3))
                {
                    case BlockState.I:
                        kickOffsets = _tetrominoIKickOffsets;
                        break;
                    case BlockState.O:
                    case BlockState.Plus:
                        // These blocks have no offsets as they can't be rotated.
                        break;
                    default:
                        // Corner block doesn't work well with these though.
                        kickOffsets = _tetrominoJLSTZKickOffsets;
                        break;
                }
                if (kickOffsets != null)
                {
                    // Go through the valid kick tests for the current rotation and direction.
                    int rotationDirection = direction == 1 ? 1 : 0;
                    for (int kickOffset = 0; kickOffset < kickOffsets[0][0].Length; kickOffset++)
                    {
                        if (!CheckTetrominoCollision(_tetrominoIndex, nextRotation,
                            _tetrominoOffset + kickOffsets[_tetrominoRotation][rotationDirection][kickOffset]))
                        {
                            canRotate = true;
                            nextOffset = kickOffsets[_tetrominoRotation][rotationDirection][kickOffset];
                            break;
                        }
                    }
                }
            }

            // If the block can rotate, set the new rotation and optional offset through SRS.
            if (canRotate)
            {
                _tetrominoRotation = nextRotation;
                _tetrominoOffset += nextOffset;
                UpdateDropTetromino();

                DrawBoard();
            }
        }

        private void LowerTetromino()
        {
            // Check if the moving tetromino collides and freeze it if it does.
            if (CheckTetrominoCollision(_tetrominoIndex, _tetrominoRotation, _tetrominoOffset + _settings.BoardWidth))
            {
                FreezeTetromino();
            }
            else
            {
                _tetrominoOffset += _settings.BoardWidth;
                DrawBoard();
            }
        }

        private void FreezeTetromino()
        {
            // Freeze the tetromino at the bottom most position.
            _tetrominoOffset = _dropOffset;
            foreach (int offset in _tetrominoes[_tetrominoIndex][_tetrominoRotation])
                _board[offset + _tetrominoOffset] = (BlockState)_tetrominoIndex + 3;

            // Destroy complete lines and add a new tetromino.
            DestroyFullLines();
            NewTetromino();

            DrawBoard();
        }

        private void DestroyFullLines()
        {
            int lineCount = 0;
            // Check for complete lines and remove them.
            for (int y = _board.Length - _settings.BoardWidth * 2; y >= 0; y -= _settings.BoardWidth)
            {
                bool isCompleteRow = true;
                for (int x = y + 1; x < y + _settings.BoardWidth - 1; x++)
                {
                    if (_board[x] == BlockState.None)
                    {
                        isCompleteRow = false;
                        break;
                    }
                }
                // This row is complete, lower the blocks above.
                if (isCompleteRow)
                {
                    lineCount++;
                    for (int aboveY = y - _settings.BoardWidth; aboveY >= 0; aboveY -= _settings.BoardWidth)
                        for (int x = aboveY + 1; x < aboveY + _settings.BoardWidth - 1; x++)
                            _board[x + _settings.BoardWidth] = _board[x];
                    // Set y back one line to recheck it (multiple lines are complete).
                    y += _settings.BoardWidth;
                }
            }

            // Receive points for the amount of lines / level.
            switch (lineCount)
            {
                case 1:
                    _score += 4 * ((_maxDelay - _delay) / 10);
                    break;
                case 2:
                    _score += 10 * ((_maxDelay - _delay) / 10);
                    break;
                case 3:
                    _score += 30 * ((_maxDelay - _delay) / 10);
                    break;
                case 4:
                    _score += 120 * ((_maxDelay - _delay) / 10);
                    break;
            }
            _lines += lineCount;

            // Increase the game speed.
            SetDelay(_delay - _delayStep * lineCount);
        }

        private bool CheckTetrominoCollision(int index, int rotation, int offset)
        {
            foreach (int tetrominoOffset in _tetrominoes[index][rotation])
            {
                if (_board[tetrominoOffset + offset] != BlockState.None)
                    return true;
            }
            return false;
        }

        private void SetDelay(int delay)
        {
            _delay = Math.Max(_minDelay, delay);
            _timer.Interval = _delay;
        }

        private void PauseOrResumeGame(bool pause)
        {
            _paused = pause;
            _timer.Enabled = !pause;
            DrawScores();
        }

        private void GameOver()
        {
            _timer.Enabled = false;

            // Prevent the appearance of "PAUSED" in the titlebar.
            _form.LostFocus -= _form_LostFocus;
            _form.GotFocus -= _form_GotFocus;

            MessageBox.Show("Game Over!", "GdiBlocks", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            _form.Close();
        }

        // ---- Drawing ----

        private void ResizeBoard()
        {
            // Get the block size according to the window layout.
            _blockSize = _form.ClientSize.Height / (_settings.BoardHeight - _vanishRows);
            if (_blockSize * _settings.BoardWidth > _form.ClientSize.Width)
                _blockSize = Math.Max(1, _form.ClientSize.Width / _settings.BoardWidth);

            if (_blockSize > 0)
            {
                // Recreate the bitmap according to the new sizes.
                _bitmap = new Bitmap(_blockSize * _settings.BoardWidth,
                    _blockSize * (_settings.BoardHeight - _vanishRows));
                _graphics = Graphics.FromImage(_bitmap);
                _graphics.CompositingMode = CompositingMode.SourceCopy;
                _graphics.CompositingQuality = CompositingQuality.HighSpeed;
                _graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                _graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                _graphics.SmoothingMode = SmoothingMode.HighSpeed;
            }
        }

        private void DrawBoard()
        {
            if (_blockSize > 0)
            {
                // Draw the overlays.
                DrawStatics();
                DrawDropTetromino();
                DrawTetromino();
                DrawScores();

                // Now draw the new bitmap on the form.
                _form.Refresh();
            }
        }

        private void DrawStatics()
        {
            // Don't draw the vanish zone.
            for (int i = _vanishRows * _settings.BoardWidth; i < _board.Length; i++)
            {
                int x = i % _settings.BoardWidth;
                int y = i / _settings.BoardWidth;
                DrawBlock(x, y, _board[i]);
            }
        }

        private void DrawTetromino()
        {
            foreach (int offset in _tetrominoes[_tetrominoIndex][_tetrominoRotation])
            {
                int realOffset = offset + _tetrominoOffset;
                DrawBlock(realOffset % _settings.BoardWidth, realOffset / _settings.BoardWidth,
                    (BlockState)_tetrominoIndex + 3);
            }
        }

        private void DrawDropTetromino()
        {
            if (!_settings.GhostTile)
                return;

            foreach (int offset in _tetrominoes[_tetrominoIndex][_tetrominoRotation])
            {
                int realOffset = offset + _dropOffset;
                DrawBlock(realOffset % _settings.BoardWidth, realOffset / _settings.BoardWidth,
                    BlockState.Ghost);
            }
        }

        private void DrawScores()
        {
            _form.Text = (_paused ? "PAUSED | " : String.Empty)
                + $"Score: {_score} | Blocks: {_blocks} | Lines: {_lines} | Speed: {(_maxDelay - _delay) / 10}";
        }

        private void DrawBlock(int x, int y, BlockState blockState)
        {
            // Get the colors.
            Color color = GetBlockColor(blockState);
            Color colorDark = Color.FromArgb(255, color.R / 2, color.G / 2, color.B / 2);

            Rectangle rectangle = new Rectangle(x * _blockSize, (y - _vanishRows) * _blockSize,
                _blockSize, _blockSize);

            // Draw with a linear gradient.
            using (LinearGradientBrush lgBr = new LinearGradientBrush(rectangle, color, colorDark,
                LinearGradientMode.ForwardDiagonal))
            {
                _graphics.FillRectangle(lgBr, rectangle);
            }
        }

        private Color GetBlockColor(BlockState blockState)
        {
            switch (blockState)
            {
                case BlockState.Ghost:
                    return Color.FromArgb(50, 50, 50);
                case BlockState.Border:
                    return Color.Silver;
                case BlockState.I:
                    return Color.CornflowerBlue;
                case BlockState.J:
                    return Color.Blue;
                case BlockState.L:
                    return Color.Orange;
                case BlockState.O:
                    return Color.Yellow;
                case BlockState.S:
                    return Color.Green;
                case BlockState.T:
                    return Color.Magenta;
                case BlockState.Z:
                    return Color.Red;
                case BlockState.U:
                    return Color.LightGreen;
                case BlockState.Plus:
                    return Color.Cyan;
                case BlockState.LongS:
                    return Color.Violet;
                case BlockState.LongZ:
                    return Color.Wheat;
                case BlockState.Corner:
                    return Color.Brown;
                default:
                    return Color.FromArgb(30, 30, 30);
            }
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void _form_LostFocus(object sender, EventArgs e)
        {
            if (!_paused)
            {
                _pausedThroughDeactivation = true;
                PauseOrResumeGame(true);
            }
        }

        private void _form_GotFocus(object sender, EventArgs e)
        {
            if (_pausedThroughDeactivation)
            {
                _pausedThroughDeactivation = false;
                PauseOrResumeGame(false);
            }
        }

        private void _form_KeyDown(object sender, KeyEventArgs e)
        {
            if (!_paused)
            {
                // Keys allowed in play.
                switch (e.KeyCode)
                {
                    case Keys.Left:
                        MoveTetromino(-1);
                        break;
                    case Keys.Right:
                        MoveTetromino(1);
                        break;
                    case Keys.Up:
                        RotateTetromino(1);
                        break;
                    case Keys.Down:
                        LowerTetromino();
                        break;
                    case Keys.Space:
                        FreezeTetromino();
                        break;
                }
            }

            // Keys allowed in pause or play.
            switch (e.KeyCode)
            {
                case Keys.P:
                case Keys.Pause:
                    PauseOrResumeGame(!_paused);
                    break;
                case Keys.Escape:
                    GameOver();
                    break;
            }
        }

        private void _form_Paint(object sender, PaintEventArgs e)
        {
            // Draw the board centered.
            e.Graphics.DrawImageUnscaled(_bitmap, new Rectangle(
                _form.ClientSize.Width / 2 - _bitmap.Width / 2,
                _form.ClientSize.Height / 2 - _bitmap.Height / 2,
                _bitmap.Width, _bitmap.Height));
        }

        private void _form_SizeChanged(object sender, EventArgs e)
        {
            if (!_paused && _form.WindowState == FormWindowState.Minimized)
            {
                _pausedThroughDeactivation = true;
                PauseOrResumeGame(true);
            }
            else if (_pausedThroughDeactivation && _form.WindowState != FormWindowState.Minimized)
            {
                _pausedThroughDeactivation = false;
                PauseOrResumeGame(false);
            }

            ResizeBoard();
            DrawBoard();
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            LowerTetromino();
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private enum BlockState : byte
        {
            None, Ghost, Border, I, J, L, O, S, T, Z,
            // Extended blocks
            U, Plus, LongS, LongZ, Corner
        }
    }
}
