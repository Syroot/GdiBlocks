﻿namespace Syroot.GdiBlocks
{
    /// <summary>
    /// Provides a group of settings which define the game startup behavior.
    /// </summary>
    internal class GameSettings
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the width of the game board.
        /// </summary>
        internal int BoardWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the game board.
        /// </summary>
        internal int BoardHeight { get; set; }

        /// <summary>
        /// Gets or sets a value determining whether an extended set of tetrominoes is available.
        /// </summary>
        internal bool ExtendedTetrominoes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a ghost tile will be shown.
        /// </summary>
        internal bool GhostTile { get; set; }

        /// <summary>
        /// Gets or sets the maximum possible game speed.
        /// </summary>
        internal int MaximumSpeed { get; set; }

        /// <summary>
        /// Gets or sets the startup speed.
        /// </summary>
        internal int StartupSpeed { get; set; }
    }
}
