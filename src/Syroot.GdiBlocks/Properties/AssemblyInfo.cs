﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GdiBlocks")]
[assembly: AssemblyDescription("GDI+ Tetris Clone")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("GdiBlocks")]
[assembly: AssemblyCopyright("(c) Syroot, licensed under MIT")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("de0aefc1-66a0-4f94-959e-55b90a3c7a4a")]
